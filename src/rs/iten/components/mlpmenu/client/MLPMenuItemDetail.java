/**
 * Copyright 2015 ITEN Engineering d.o.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rs.iten.components.mlpmenu.client;

import com.google.gwt.dom.client.AnchorElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.LIElement;
import com.google.gwt.user.client.ui.Widget;

/**
 * Class that represent item detail. Contains details object and command that is executed on click event.
 * Detail object should have toString() method overridden as it is used for display.
 * 
 * @author Marko Nikolić
 *
 */
public class MLPMenuItemDetail extends Widget {
	/**
	 * Details information. Must provide toString method.
	 */
	private Object detail;
	
	/**
	 * Command to be executed on click on detail.
	 */
	private MLPMenuCommand detailCommand;
	
	/**
	 * Element representing detail.
	 */
	private LIElement listItemDetail;
	
	/**
	 * Creates new menu item detail with given object and command.
	 * 
	 * @param detail detail object
	 * @param detailCommand command executed on click event
	 */
	public MLPMenuItemDetail(Object detail, MLPMenuCommand detailCommand) {
		this.detail = detail;
		this.detailCommand = detailCommand;
		
		createDetail();
		setElement(listItemDetail);
	}
	
	/**
	 * Returns details object.
	 * 
	 * @return details object
	 */
	public Object getDetail() {
		return detail;
	}
	
	/**
	 * Return command object.
	 * 
	 * @return command object.
	 */
	public MLPMenuCommand getDetailCommand() {
		return detailCommand;
	}
	
	/**
	 * Creates item details elements.
	 */
	private void createDetail() {
		listItemDetail = Document.get().createLIElement();
		AnchorElement anchorDetail = Document.get().createAnchorElement();
		anchorDetail.setClassName("mlp-menu-item-details");
		anchorDetail.setHref("#");
		anchorDetail.setInnerText(detail.toString());
		listItemDetail.appendChild(anchorDetail);
	}
}
