/**
 * Copyright 2015 ITEN Engineering d.o.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rs.iten.components.mlpmenu.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import rs.iten.components.mlpmenu.resources.Resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.AnchorElement;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.HeadingElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.UListElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * <p>
 * {@link MLPMenu} is a class that represents Multi Level Push Menu.
 * </p>
 * <p>
 *  Menu contains of menu levels, where each menu level contains menu items ({@link MLPMenuItem}). When {@link MLPMenuItem} is added to
 *  the {@link MLPMenu}, it is added directly to the root level.
 *  </p>
 *  Each {@link MLPMenuItem} can contain sub-items. Sub-items are of type {@link MLPMenuItem}. If {@link MLPMenuItem} has sub items,
 *  it is considered as a menu sub-level. Initially, root level is expanded.<br>
 *  When menu sub-level is clicked with mouse, it is sliding to the right and sub-level {@link MLPMenuItem}s are shown.
 *  </p>
 *  Each {@link MLPMenuItem} has actions and details assigned.<br>
 *  Actions is a command that is executed when menu item is selected.<br>
 *  Details are list of object presented when + sign is clicked. If details are present, {@link MLPMenuItem} is displayed with + sign.
 *  Details also can have command assigned, which is executed on click event.
 *  </p>
 *  
 * @author Marko Nikolić
 *
 */
public class MLPMenu extends Composite implements HasSelectionHandlers<MLPMenuItem>{
	
	private static MLPMenuUiBinder uiBinder = GWT.create(MLPMenuUiBinder.class);

	interface MLPMenuUiBinder extends UiBinder<Widget, MLPMenu> {
	}

	@UiField Anchor btnBack;
	@UiField HeadingElement menuHeading;
	@UiField UListElement menuContent;
	
	/**
	 * Root object of the menu. This object is not visible and serves
	 * as a container for other menu items. All contained items in the
	 * root object are displayed on the first menu level.
	 * 
	 * Also, root object holds title of the menu - set/getText on the
	 * menu returns root object user data.
	 */
	@UiField MLPMenuItem root;
	
	/**
	 * Path of the selected object
	 */
	private Stack<MLPMenuItem> breadCrumb = new Stack<MLPMenuItem>();
	
	/**
	 * Opened menu item.
	 */
	private MLPMenuItem openedItem;

	/**
	 * Selected menu item.
	 */
	private MLPMenuItem selectedItem;
	
	/**
	 * <p>
	 * Option to control if leaves are put in breadcrumb.
	 * </p>
	 * <p>
	 * If openLeaves is set to true, openedItem and selectedItem will always be the same.
	 * If openLeaves is set to false, openedItem and selectedItem will be the same except when leaf item is selected.
	 * In this case, openedItem will point to the leaf parent, while selectedItem will be set to leaf. Clicking on back
	 * will again set openedItem and selectedItem to the same value.
	 * </p>
	 */
	private boolean openLeaves = false;
	
	/**
	 * Option for allowing usage of short codes instead of toString representation of an object.
	 */
	private boolean useCodes = false;
	
	/**
	 * Creates default {@link MLPMenu}
	 */
	public MLPMenu() {
		this(null);
	}
	
	/**
	 * Creates {@link MLPMenu} with specified header text.
	 * 
	 * @param text text that will be shown in header.
	 */
	public MLPMenu(String text) {
		this(text, Resources.defaultResources);
	}

	/**
	 * Creates {@link MLPMenu with specified header text and styling}
	 * 
	 * @param text text that will be shown in header.
	 * @param resources styling that will be applied to the {@link MLPMenu}
	 */
	public MLPMenu(String text, Resources resources) {
		resources.mlpMenuCss().ensureInjected();
		
		initWidget(uiBinder.createAndBindUi(this));
		
		// set default style
		root.setStyleName("mlp-menu-level-item");

		openedItem = root;
		breadCrumb.push(root);
		root.open();
		selectedItem = root;
		
		setUserObject(text);
		
		sinkEvents(Event.ONCLICK);
	}

	/**
	 * Adds item to the menu root
	 * 
	 * @param item item to be added
	 */
	public void addItem(final MLPMenuItem item) {
		root.addItem(item);
	}

	/**
	 * Removes given item from the menu
	 * 
	 * @param item item to be removed
	 * @return boolean, success or failure
	 */
	public boolean removeItem(MLPMenuItem item) {
		return root.removeItemRecursively(item);
	}
	
	/**
	 * Clears all items from the menu
	 */
	public void clear() {
		root.clear();
	}

	/**
	 * Finds item that contains gives user object.
	 * 
	 * It is required that user object have properly overloaded
	 * equal() method.
	 * 
	 * @param userObject
	 * @return item with given user object or null if not found.
	 */
	public MLPMenuItem findItem(Object userObject) {
		return root.findItem(userObject);
	}
	
	/**
	 * Returns menu user object
	 */
	public Object getUserObject() {
		return root.getUserObject();
	}

	/**
	 * Sets menu user object
	 */
	public void setUserObject(Object userObject) {
		root.setUserObject(userObject);
		updateBreadCrumb();
	}	
	
	/**
	 * Updates breadcrumb, displays path of the opened menu.
	 */
	public void updateBreadCrumb() {
		String path = "";
		Iterator<MLPMenuItem> i = breadCrumb.iterator();
		String pathPart = "";
		
		while(i.hasNext()) {
			Object userObject = i.next().getUserObject();
			
			if (userObject != null)
				if (useCodes && (userObject instanceof HasCodes))
					pathPart = ((HasCodes)userObject).toCode() != null ? ((HasCodes)userObject).toCode() : userObject.toString();
				else
					pathPart = userObject.toString();
			else
				pathPart = "";
					
			path = path + pathPart + " / ";
		}
		menuHeading.setInnerText(path);
	}
	
	/**
	 * Find full path from given to the opened item.
	 */	
	public Stack<MLPMenuItem> getFullPath(MLPMenuItem item) {
		Stack<MLPMenuItem> fullPath = new Stack<MLPMenuItem>();

		while (item != openedItem) {
			MLPMenuItem parentItem = item.getParentItem();
			if (parentItem != openedItem) {
				fullPath.push(parentItem);
			}
			item = parentItem;
		}

		return fullPath;
	};

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.Composite#onBrowserEvent(com.google.gwt.user.client.Event)
	 */
	@Override
	public void onBrowserEvent(Event event) {
		switch (event.getTypeInt()) {
		case Event.ONCLICK:
			// Handle only clicks on a element within items.
			Element target = Element.as(event.getEventTarget());
			if (target.getTagName().equalsIgnoreCase(AnchorElement.TAG)) {
				// Check if clicked element is menu item
				MLPMenuItem clickedItem = root.findItem(target.getParentElement());
				if (clickedItem != null) {
					openMenu(clickedItem);
					break;
				}
				
				// Check if clicked element is details item
				MLPMenuItemDetail clickedDetail = openedItem.findDetail(target.getParentElement());
				if (clickedDetail != null) {
					MLPMenuCommand cmd = clickedDetail.getDetailCommand();
					if (cmd != null) {
						cmd.execute(clickedDetail.getDetail());
					}
					break;
				}
			}
			
			
			if (target.getTagName().equalsIgnoreCase(SpanElement.TAG)) {
				// Check if clicked element is details show-more icon
				MLPMenuItem clickedToogleDetails = openedItem.findDetailToogle(target.getParentElement().getParentElement());
				if (clickedToogleDetails != null)
					clickedToogleDetails.toogleDetails();
			}
			
			break;
		}
		
		super.onBrowserEvent(event);
	}

	/**
	 * Opens menu item, if it has submenus or if it is set to open leaves.
	 * Fires selection event and executes command associated with item.
	 * 
	 * @param item
	 */
	public void openMenu(MLPMenuItem item) {
		if (item.hasChildren() || openLeaves) {
			openedItem = item;
			breadCrumb.push(openedItem);
			updateBreadCrumb();
			openedItem.open();
		}
		
		selectedItem = item;
		SelectionEvent.<MLPMenuItem>fire(this, selectedItem);
		
		MLPMenuCommand cmd = item.getUserCommand(); 
		if (cmd != null) {
			cmd.execute(item.getUserObject());
		}
	}
	
    @UiHandler("btnBack")
    void btnBackAnchorHandleClick(ClickEvent e) {
    	closeMenu();
    }

	/**
	 * Closes opened menu and fires selection event.
	 */
	public void closeMenu() {
		if (breadCrumb.size() > 1) {
        	MLPMenuItem item = breadCrumb.pop();
        	item.close();
        	
        	openedItem = selectedItem = breadCrumb.peek();

        	SelectionEvent.<MLPMenuItem>fire(this, selectedItem);
            updateBreadCrumb();
    	}
	}
    
    /**
     * Return iterator that goes over all menu items in all levels.
     * 
     * @return iterator that goes over all menu items in all levels.
     */
    public Iterator<MLPMenuItem> iterator() {
    	List<MLPMenuItem> allItems = new ArrayList<MLPMenuItem>();
    	
    	root.collectChildren(allItems);
    	
    	return allItems.iterator();
    }

	/**
	 * Returns currently opened item (sub-level).
	 * 
	 * @return the openedItem
	 */
	public MLPMenuItem getOpenedItem() {
		return openedItem;
	}

	/**
	 * Returns currently selected item.
	 * 
	 * @return the selectedItem
	 */
	public MLPMenuItem getSelectedItem() {
		return selectedItem;
	}

	/**
	 * <p>
	 * If this parameter is set, leaves will be stored within breadcrumb.
	 * </p>
	 * <p>
	 * Default is false, leaves will not be stored in breadcrumb.
	 * </p>
	 * 
	 * @param openLeaves true to have leaves in breadcrumb, false otherwise.
	 */
	public void setOpenLeaves(boolean openLeaves) {
		this.openLeaves = openLeaves;
	}
		
	@Override
	public HandlerRegistration addSelectionHandler(SelectionHandler<MLPMenuItem> handler) {
		return super.addHandler(handler, SelectionEvent.getType());
	}

	
	/**
	 * @return the useCodes
	 */
	public boolean isUseCodes() {
		return useCodes;
	}

	/**
	 * <p>
	 * Sets parameter for using short codes instead of object.toString in breadcrumb.
	 * </p>
	 * 
	 * <ul>
	 * This parameter allows using two string representations, if supported by {@link MLPMenuItem} object:
	 * <li>- one is toString() used within menu items etc...</li>
	 * <li>- the other is toCode() which can be used for breadcrumb.</li>
	 * </ul>
	 * 
	 * <p>
	 * If useCodes is configured, then breadcrumb will show short codes, if {@link MLPMenuItem} object implements {@link HasCodes} interface.
	 * </p>
	 * 
	 * @param useCodes the useCodes to set
	 */
	public void setUseCodes(boolean useCodes) {
		this.useCodes = useCodes;
	}
}

