/**
 * Copyright 2015 ITEN Engineering d.o.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rs.iten.components.mlpmenu.client;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gwt.dom.client.AnchorElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.LIElement;
import com.google.gwt.dom.client.SpanElement;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.UListElement;
import com.google.gwt.user.client.ui.Widget;

/**
 * <p>
 * Multi level push menu item implementation.
 * </p>
 * <p>
 * Item can be either a container for other items, which makes item a sub-level in {@link MLPMenu}, or it can be a leaf item.<br>
 * If item is a leaf, it can contain details represented by {@link MLPMenuItemDetail}. Details are displayed when user clicks on a
 * plus sign shown for each item that contains details.
 * </p>
 * <p>
 * Item can have user object or command assigned to it.<br>
 * User object should have toString() method overridden, as it is used as item text.<br> 
 * Command is executed on a mouse click event.
 * </p>    
 *
 * @author Marko Nikolić
 */
public class MLPMenuItem extends Widget {

	/**
	 * List of child menu items.
	 * Menu items on the same level must be unique.
	 */
	private List<MLPMenuItem> childItems = new ArrayList<MLPMenuItem>();

	/**
	 * User data contained in the menu item.
	 */
	private Object userObject;

	/**
	 * Command that should be executed on menu item selection
	 */
	private MLPMenuCommand userCommand;
	
	/**
	 * Details stored within this menu item
	 */
	private List<MLPMenuItemDetail> details = new ArrayList<MLPMenuItemDetail>();

	/**
	 * Parent menu item
	 */
	private MLPMenuItem parentItem = null;
	
	/**
	 * Span element used for displaying arrow for items with childs
	 */
	private static final SpanElement spanArrow = createArrow();
	
	private static SpanElement createArrow() {
		SpanElement arrow = Document.get().createSpanElement();
		arrow.setClassName("icon-sub-level");
		return arrow;
	}
	
	private LIElement itemNode;
	private AnchorElement itemDetailsToggle;
	private AnchorElement itemText;
	private UListElement itemChildren;
	private UListElement itemDetails;

	/**
	 * Creates new {@link MLPMenuItem}
	 */
	public MLPMenuItem() {
		this(null, null, false);
	}

	/**
	 * Creates new {@link MLPMenuItem} with given user object.
	 * 
	 * @param userObject object that will be stored with menu item.
	 */
	public MLPMenuItem(Object userObject) {
		this(userObject, null, false);
	}

	/**
	 * Creates new {@link MLPMenuItem} with given user object and command.
	 * 
	 * @param userObject object that will be stored with menu item.
	 * @param userCommand command that will be executed on click event.
	 */
	public MLPMenuItem(Object userObject, MLPMenuCommand userCommand) {
		this(userObject, userCommand, false);
	}
	
	/**
	 * Creates new {@link MLPMenuItem} with given user object, command and visibility set.
	 * 
	 * @param userObject object that will be stored with menu item.
	 * @param userCommand command that will be executed on click event.
	 * @param visible sets item visibility when created.
	 */
	public MLPMenuItem(Object userObject, MLPMenuCommand userCommand, boolean visible) {
		createItem();
		setElement(itemNode);
		if (!visible)
			itemChildren.getStyle().setLeft(-100, Unit.PCT);
		setUserObject(userObject);
		setUserCommand(userCommand);
	}
	
	/**
	 * Creates item element. Within element node there are the following elements:
	 * 
	 * - First element is A with text associated to the user object
	 * - Second element is UL, list of child items
	 */
	private void createItem() {
		itemNode = Document.get().createLIElement();
		
		itemDetailsToggle = Document.get().createAnchorElement();
		itemDetailsToggle.setClassName("show-item-content");
		itemDetailsToggle.setHref("#");
		SpanElement span = Document.get().createSpanElement();
		span.setClassName("icon-show-more");
		itemDetailsToggle.appendChild(span);
		itemDetailsToggle.getStyle().setDisplay(Display.NONE);
		
		itemText = Document.get().createAnchorElement();
		itemText.setClassName("mlp-menu-item");
		itemText.setHref("#");
	
		itemChildren = Document.get().createULElement();
		itemChildren.setClassName("mlp-menu-level");

		itemDetails = Document.get().createULElement();
		itemDetails.setClassName("item-content");
		itemDetails.getStyle().setDisplay(Display.NONE);

		itemNode.appendChild(itemDetailsToggle);
		itemNode.appendChild(itemText);
		itemNode.appendChild(itemChildren);
		itemNode.appendChild(itemDetails);
	}
	
	/**
	 * Returns user object.
	 * 
	 * @return the userObject
	 */
	public Object getUserObject() {
		return userObject;
	}

	/**
	 * Sets user object for this item.
	 * 
	 * @param userObject the userObject to set
	 */
	public void setUserObject(Object userObject) {
		this.userObject = userObject;
		
		render();
	}

	/**
	 * Render menu item text. If there are children items, arrow is rendered
	 * as well.
	 */
	public void render() {
		if (childItems.isEmpty() || parentItem == null)
			itemText.setInnerHTML(userObject != null ? userObject.toString() : "");
		else
			itemText.setInnerHTML(spanArrow.getString() + (userObject != null ? userObject.toString() : ""));
	}

	/**
	 * Adds item as a child item.
	 * 
	 * @param item - item to be added as a child. 
	 */
	public void addItem(MLPMenuItem item) {
		// Add item as a child, set back parent
		childItems.add(item);
		itemChildren.appendChild(item.getElement());
		item.setParentItem(this);
		
		// set default style
		item.setStyleName("mlp-menu-level-item");
		
		// Get menu level; this will determine z-index of the item
		int i=1;
		MLPMenuItem tempItem = item;
		while((tempItem = tempItem.getParentItem()) != null) i++;
		item.setChildZIndex(i);
		
		// Render item, add any additional ui objects like images etc...
		render();
		
		// Needed for receiving events
		item.onAttach();
	}

	/**
	 * Sets z index of the itemChildren element that contains child items.
	 * 
	 * @param zindex - zindex to be set
	 */
	private void setChildZIndex(int zindex) {
		itemChildren.getStyle().setZIndex(zindex);
	}

	/**
	 * Removes item and all its children. 
	 */
	public void removeItem() {
		childItems.clear();
		
		if (parentItem != null)
			parentItem.removeChild(this);
	}
	
	public void removeChild(MLPMenuItem item) {
		// Remove from children collection
		Iterator<MLPMenuItem> i = childItems.iterator();
		while(i.hasNext()) {
			if (i.next() == item) {
				i.remove();
				break;
			}
		}
		
		item.getElement().removeFromParent();
	}
	
	/**
	 * Removes specified item recursively. It current item equals given item, 
	 * it is removed completely; otherwise search is done over child items.
	 * 
	 * @param item item to be removed
	 * @return boolean success or failure
	 */
	public boolean removeItemRecursively(MLPMenuItem item) {
		if (this.equals(item)) {
			removeItem();
			return true;
		}
		else {
			Iterator<MLPMenuItem> i = childItems.iterator();
			
			while(i.hasNext()) {
				if (i.next().removeItemRecursively(item)) {
					render();
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Removes all child items.
	 */
	public void clear() {
		childItems.clear();
	}

	/**
	 * Search this and all subitems until item which has
	 * provided element as underlying DOM element is found.
	 * 
	 * @param element element to be found
	 * @return item that contains element or null
	 */
	public MLPMenuItem findItem(Element element) {
		if (getElement() == element)
			return this;
		
		for(MLPMenuItem item : childItems) {
			MLPMenuItem child = item.findItem(element);
			if (child != null)
				return child;
		}
		
		return null;
	}
	
	/**
	 * Search this and all sub-items until item with given
	 * user data if found.
	 * It is required that user data have properly overloaded
	 * equal() method.
	 * 
	 * @param objectToFind Object to find
	 * @return item that contains object or null
	 */
	public MLPMenuItem findItem(Object objectToFind) {
		if (userObject != null && userObject.equals(objectToFind))
			return this;
		
		for(MLPMenuItem item : childItems) {
			MLPMenuItem child = item.findItem(objectToFind);
			if (child != null)
				return child;
		}
		
		return null;
	}
	
	/**
	 * Search this and all sub-items until item with given user string if found.
	 * 
	 * @param objectToFind
	 *            Object to find
	 * @return item that contains object or null
	 */
	public ArrayList<MLPMenuItem> findItem(String objectToFind) {
		ArrayList<MLPMenuItem> listOfFoundItems = new ArrayList<MLPMenuItem>();
		if (userObject != null
				&& userObject.toString().toLowerCase().startsWith(objectToFind)) {
			listOfFoundItems.add(this);
		}

		for (MLPMenuItem item : childItems) {
			ArrayList<MLPMenuItem> child = item.findItem(objectToFind);
			if (child != null) {
				listOfFoundItems.addAll(child);
			}
		}

		return listOfFoundItems;
	}
	
	/**
	 * @return the parentItem
	 */
	public MLPMenuItem getParentItem() {
		return parentItem;
	}

	/**
	 * @param parentItem the parentItem to set
	 */
	protected void setParentItem(MLPMenuItem parentItem) {
		this.parentItem = parentItem;
	}

	/**
	 * Show all items in the menu
	 */
	public void open() {
		itemChildren.getStyle().setLeft(0, Unit.PCT);
	}	

	/**
	 * Hides all items in the menu
	 */
	public void close() {
		itemChildren.getStyle().setLeft(-100, Unit.PCT);
	}

	/**
	 * Returns true if item has children, false otherwise
	 * 
	 * @return true if item has children, false otherwise
	 */
	public boolean hasChildren() {
		return !childItems.isEmpty();
	}

	/**
	 * Returns children of this item. Only direct children are returned, not
	 * children of the children.
	 * 
	 * @return list of {@link MLPMenuItem}
	 */
	public List<MLPMenuItem> getChildren() {
		return childItems;
	}
	
	/**
	 * Returns collection of all children of this item. Children are collected
	 * recursively, all the way bellow to the leaves of the menu.
	 * 
	 * @param container list of {@link MLPMenuItem}s where children will be stored
	 */
	public void collectChildren(List<MLPMenuItem> container) {
		container.add(this);
		
		for(MLPMenuItem item : childItems)
			item.collectChildren(container);
	}

	/**
	 * Returns command associated with menu item.
	 * 
	 * @return the userCommand
	 */
	public MLPMenuCommand getUserCommand() {
		return userCommand;
	}

	/**
	 * Sets command associated to the menu item.
	 * 
	 * @param userCommand the userCommand to set
	 */
	public void setUserCommand(MLPMenuCommand userCommand) {
		this.userCommand = userCommand;
	}

	/**
	 * Adds detail to this menu item
	 * 
	 * @param detail to add
	 * @param command to execute on detail click
	 */
	public void addDetail(Object detail, MLPMenuCommand command) {
		MLPMenuItemDetail newDetail = new MLPMenuItemDetail(detail, command); 
		details.add(newDetail);
		itemDetails.appendChild(newDetail.getElement());
		
		itemDetailsToggle.getStyle().setDisplay(Display.BLOCK);
	}
	
	/**
	 * Removes all details assigned to this item.
	 */
	public void removeDetails() {
		details.clear();
		itemDetailsToggle.getStyle().setDisplay(Display.NONE);
	}

	/**
	 * Used internally for iterating through list of details.
	 * 
	 * @return list of details
	 */
	private List<MLPMenuItemDetail> getDetails() {
		return details;
	}
	
	/**
	 * Finds detail represented by given element. Details in child items are checked,
	 * no recursive check is made.
	 * 
	 * @param target - given element
	 * @return Menu item detail or null if element does not match any detail within item
	 */
	public MLPMenuItemDetail findDetail(Element target) {
		for (MLPMenuItem item : childItems) {
			for(MLPMenuItemDetail detail : item.getDetails()) {
				if (detail.getElement() == target)
					return detail;
			}
		}
		
		return null;
	}

	/**
	 * Finds item within children whose toggle button is equal to given element.
	 * 
	 * @param element given element
	 * @return item that contains details as element or null
	 */
	public MLPMenuItem findDetailToogle(Element element) {
		for (MLPMenuItem item : childItems) {
			if (item.getElement().equals(element))
				return item;
		}
		
		return null;
	}

	/**
	 * Toggles display of details for this item.
	 */
	public void toogleDetails() {
		if (itemDetails.getStyle().getDisplay().equalsIgnoreCase(Display.NONE.toString())) {
			itemDetails.getStyle().setDisplay(Display.BLOCK);
			itemNode.addClassName("expanded");
		}
		else {
			itemNode.removeClassName("expanded");
			itemDetails.getStyle().setDisplay(Display.NONE);
		}
	}
}
