/**
 * Copyright 2015 ITEN Engineering d.o.o.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package rs.iten.components.mlpmenu.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;

/**
 * Resources used by MLPMenu
 * 
 * @author Marko Nikolić
 *
 */
public interface Resources extends ClientBundle {

	public static final Resources defaultResources = GWT.create(Resources.class);

	@Source("css/mlpmenu.css")
	public CssResource mlpMenuCss();
	
	@Source("img/icon-show-more.png")
	@ImageOptions(repeatStyle = RepeatStyle.None)
	public ImageResource iconShowMore();
	
	@Source("img/level-icon.png")
	@ImageOptions(repeatStyle = RepeatStyle.None)
	public ImageResource iconLevel();
}

