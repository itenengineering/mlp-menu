# Multi Level Push Menu - GWT #

This is an implementation of a Multi Level Push Menu (mlp-menu) for Google Web Toolkit.

MLP Menu is made as GWT component and is inspired by [this](https://github.com/adgsm/multi-level-push-menu) JQuery plugin. Implemented functionality is sufficient for basic MLP Menu handling, as we needed for our projects. Hopefully it will grow over time.

Sample image of the menu is provided below and demo can be found [here](http://www.iten.rs/multi-level-push-menu-for-gwt).

![mlpm-demo.png](https://bitbucket.org/repo/6jo8g4/images/4046098050-mlpm-demo.png)

This software is released under Apache License Version 2.0 (see LICENCE file).